package com.udemy.backendninja.constant;

public class ViewConstant {

	public static final String CONTACT_FORM_VIEW = "contactform";
	public static final String CONTACTS_LIST_VIEW = "contacts";
	public static final String LOGIN_FORM_VIEW = "login";
}
